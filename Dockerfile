FROM docker.io/rust:1.59 as sevctl
RUN cargo install --root /usr/local --bin sevctl -- sevctl

# FROM docker.io/rust:1.59 as enarx
# RUN apt-get update && apt-get install -y \
#     musl-tools \
#  && rm -rf /var/lib/apt/lists/*
# RUN rustup toolchain install nightly-2022-03-14 -t x86_64-unknown-linux-musl,x86_64-unknown-linux-gnu
# RUN cargo +nightly-2022-03-14 install --root /usr/local --bin enarx -- enarx

FROM registry.gitlab.com/enarx/lab:latest

# Install and configure the kernel
ADD linux.tar.bz2 /
RUN cd /boot; ln -s vmlinuz* wyrcan.kernel

# Install SEV firmware
COPY amd_sev_fam17h_model0xh.sbin /lib/firmware/amd/
COPY amd_sev_fam17h_model3xh.sbin /lib/firmware/amd/
COPY amd_sev_fam19h_model0xh.sbin /lib/firmware/amd/

# Enable SEV and set SEV limits
COPY kvm-amd.conf /etc/modprobe.d/kvm-amd.conf
COPY sev.conf /etc/security/limits.d/sev.conf
COPY 99-sev.rules /etc/udev/rules.d/

# Compile sevctl and install service
COPY --from=sevctl /usr/local/bin/sevctl /usr/local/bin/
COPY sevctl.service /etc/systemd/system/sevctl.service
RUN mkdir -p /var/cache/amd-sev \
    && systemctl enable sevctl.service

# Ensure VCEK is updated
COPY --chown=root:root --chmod=700 enarx /sbin/enarx
# COPY --from=enarx --chown=root:root --chmod=700 /usr/local/bin/enarx /sbin/enarx
COPY vcek-update.service /etc/systemd/system/vcek-update.service
RUN systemctl enable vcek-update.service
